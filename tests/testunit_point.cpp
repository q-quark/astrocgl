#define BOOST_TEST_MODULE Point test
#include <boost/test/included/unit_test.hpp>

#include "Point.h"

BOOST_AUTO_TEST_CASE( constructors_test )
{
// Check Default Constructors
    // check strange Dim construct
    AstroCGL::Point<0,int> p0;
    BOOST_CHECK( p0.isNull() );

    // check types default initialization values
    AstroCGL::Point<1,int> p1;
    BOOST_CHECK( p1.isNull() );

    AstroCGL::Point<1,float> p2;
    BOOST_CHECK( p2.isNull() );

    AstroCGL::Point<1,double> p3;
    BOOST_CHECK( p3.isNull() );

// check Explicit Constructors
    AstroCGL::Point<1,int> p01(71);
    BOOST_CHECK( !p01.isNull() );

    AstroCGL::Point<2,float> p02(71, 49);
    BOOST_CHECK( !p02.isNull() );

    AstroCGL::Point<3,double> p03(71, 49, 15);
    BOOST_CHECK( !p03.isNull() );

}

BOOST_AUTO_TEST_CASE( methods_test )
{
    AstroCGL::Point<1,int> p01(71);
    BOOST_CHECK( !p01.isNull() );
    BOOST_REQUIRE_NO_THROW(p01.setX(80));
    BOOST_CHECK_EQUAL(p01[0], 80);
    BOOST_CHECK_EQUAL(p01.dim(), 1);
    BOOST_CHECK_EQUAL(p01.data()[0], 80);
    BOOST_CHECK_EQUAL(p01.array()->at(0), 80);
    BOOST_CHECK_EQUAL(p01.x(), 80);
    BOOST_CHECK_EQUAL(p01.manhattanLength(), 80);
    BOOST_CHECK_EQUAL(p01.length(), 80);

    AstroCGL::Point<2,float> p02(71,49);
    BOOST_CHECK_EQUAL(p02.dim(), 2);
    BOOST_CHECK( !p02.isNull() );
    BOOST_CHECK_NO_THROW(p02.setX(3));
    BOOST_CHECK_NO_THROW(p02.setY(4));
    BOOST_CHECK_EQUAL(p02[0], 3);
    BOOST_CHECK_EQUAL(p02.data()[0], 3);
    BOOST_CHECK_EQUAL(p02.array()->at(0), 3);
    BOOST_CHECK_EQUAL(p02.x(), 3);

    BOOST_CHECK_EQUAL(p02[1], 4);
    BOOST_CHECK_EQUAL(p02.data()[1], 4);
    BOOST_CHECK_EQUAL(p02.array()->at(1), 4);
    BOOST_CHECK_EQUAL(p02.y(), 4);

    BOOST_CHECK_EQUAL(p02.manhattanLength(), 7);
    BOOST_CHECK_EQUAL(p02.length(), 5);
}
