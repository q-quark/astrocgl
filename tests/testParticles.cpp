#include <iostream>

#include "Body.h"
#include "Point.h"
#include "Vector.h"

int main(int argc, char const *argv[])
{

    AstroCGL::ClassicParticle<2,float> particle;

    particle.setMass(2);
    particle.setPosition( AstroCGL::Point<2,float>{3,4} );
    particle.setVelocity( AstroCGL::Vector<2,float>{5,0} );

    std::cout << "Particle mass:" << particle.mass() << " position:"<< particle.position().length() << " velocity:" << particle.velocity().length()
              << " Energy:" << particle.energy() << " Momentum:" << particle.momentum().length()  << std::endl;

    return 0;
}
