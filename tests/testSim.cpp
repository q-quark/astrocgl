#include <iostream>

#include "Body.h"
#include "World.h"

#include "SimpleAlgorithm.h"
#include "SimEngine.h"

using namespace AstroCGL;

template < std::size_t Dim, typename T >
void printBody(const SharedBody< Dim,T >& b)
{
    std::cout << "mass=" << b->mass() << " position(" <<
              b->position().x() << ", " <<
              b->position().y() <<", "  <<
              b->position().z() <<") velocity(" <<

                 b->velocity().x() << ", " <<
                 b->velocity().y() <<", "  <<
                 b->velocity().z() <<") " <<
                 "energy=" << b->energy() << std::endl;

}

int main(int argc, char const *argv[])
{
    std::cout<< "Starting allocation objects..." <<std::endl;

//    Body<3,float, ClassicParticle> *p1 = new ClassicParticle<3,float>();
    SharedBody<3,float> p1(new ClassicParticle<3,float>());
    p1->setMass(10.0);
    p1->setPosition( {-1,0,1} );
    p1->setVelocity( {2,0,9} );

    std::cout<< "Setting world..." <<std::endl;
    World<3,float> myWorld; // myWorld
    myWorld.setPoints(Point<3,float>(-1,-1,-1), Point<3,float>(100,100,100));

    std::cout<< p1 << std::endl;
    printBody(p1);

    std::cout<< "Adding objects to the myWorld..." <<std::endl;
    myWorld.addBody( p1  );

    std::cout << "Does myWorld contains particle? :" << myWorld.contains(p1) << std::endl;

    myWorld.body(0)->setMass(16.0);
    myWorld.body(0)->setVelocity( {20.0, .0, .0} );

    printBody(p1);

/*
	BarnHutAlgorithm bhAlgo;
	// SimpleNewton stnewtonAlgo;
	FieldNewton g;
	bhAlgo.addField(g);
*/

    SimpleAlgorithm<3,float>* simpleNewtonAlg = new SimpleAlgorithm<3,float>();
    FieldNewton<3,float>* g = new FieldNewton<3,float>();
    simpleNewtonAlg->setInteractionField(g);

    SimEngine<3,float> sim;
    sim.config().setStartTime(2.0);
    sim.config().setEndTime(20.0);
    sim.config().setNumberOfEvents(100);
    sim.config().addAlgorithm(simpleNewtonAlg);

    sim.setWorld(myWorld);

    sim.run();


    std::cout<< "testSim:  Ended!" << std::endl;

	return 0;
}
