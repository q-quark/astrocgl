#include <iostream>

#include "Point.h"
#include "Vector.h"

#include "TestUnits.h"

//using namespace AstroGL;

int main(int argc, char const *argv[])
{
    AstroCGL::Point<2,int> p(0,0);
    AstroCGL::Point<2,int> q(12,12);
    AstroCGL::Point<3,double> r(0,0,0);

    AstroCGL::Vector<2,int> v(3,4);
    AstroCGL::Vector<3,double> u(1,3,5);

    r.moveBy(u);

    std::cout << "Verificam egalitatea a punctelor P si Q..." << std::endl;
    if(p==q)
        std::cout << "Punctele P si Q - Sunt egale" << std::endl;
    else
        std::cout << "Punctele P si Q - NU sunt egale" << std::endl;

    std::cout << "V.Norm: " << v.length() << std::endl;

    // check difference
    q-p;

    //check vector addition with a scalar
    v+6;

    AstroCGL::Vector<2,int> w = q-p;
    std::cout << "w: " << w.length() << std::endl;


    return 0;
}
