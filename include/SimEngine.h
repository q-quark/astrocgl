#ifndef SIM_ENGINE_H__
#define SIM_ENGINE_H__ 

#include <string>

#include "World.h"

#include "SimAlgorithm.h"
#include "SimEvent.h"
#include "SimEngineConfig.h"

namespace AstroCGL
{

template <std::size_t Dim,typename T>
class SimEngine
{
public:
    SimEngine();
    virtual ~SimEngine();

    std::string outputFileName() const;

    void setOutputFileName(const std::string &outputFileName);
    void loadConfigFromFile(const std::string &configFileName);

    void run(); // runs the simulation loop

    SimEngineConfig<Dim, T> config() const;
    SimEngineConfig<Dim, T> config();

    void setWorld(World<Dim,T> &world);


protected:
    std::string mOutputFileName;

    SimEngineConfig<Dim,T> mConfig;
    SimEvent<Dim,T> mCurrentEvent; // current simulated event
    SimAlgorithmsList<Dim,T> mAlgs;
};

//_____________________ Implementation
template<std::size_t Dim, typename T>
SimEngine<Dim,T>::SimEngine()
{

}

template<std::size_t Dim, typename T>
SimEngine<Dim,T>::~SimEngine()
{

}

template<std::size_t Dim, typename T>
std::string SimEngine<Dim,T>::outputFileName() const
{
    return mOutputFileName;
}

template<std::size_t Dim, typename T>
void SimEngine<Dim,T>::setOutputFileName(const std::string &outputFileName)
{
    mOutputFileName = outputFileName;
}

template<std::size_t Dim, typename T>
void SimEngine<Dim,T>::loadConfigFromFile(const std::string &configFileName)
{

}

template<std::size_t Dim, typename T>
void SimEngine<Dim,T>::run()
{
    SimAlgorithm<Dim,T>* currAlgorithm;

    // preparing Algorithms for Simulation
    for(int j=0; j< config().algorithms().size(); j++)
    {
        currAlgorithm = config().algorithms().at(j);
        currAlgorithm->beginSim(mConfig);
    }

    // doing the Simulation
    for(int i=0; i< config().numberOfEvents(); i++)
    {
        mCurrentEvent.mStepIndex = i;

        for(int j=0; j< config().algorithms().size(); j++)
        {
            currAlgorithm = config().algorithms().at(j);
            currAlgorithm->processEvent(mCurrentEvent);
        }
    }

    // Algorithms prepare ending Simulation
    for(int j=0; j< config().algorithms().size(); j++)
    {
        currAlgorithm = config().algorithms().at(j);
        currAlgorithm->endSim();
    }

}

template<std::size_t Dim, typename T>
SimEngineConfig<Dim, T> SimEngine<Dim,T>::config() const
{
    return mConfig;
}

template<std::size_t Dim, typename T>
SimEngineConfig<Dim, T> SimEngine<Dim,T>::config()
{
    return mConfig;
}

template<std::size_t Dim, typename T>
void SimEngine<Dim,T>::setWorld(World<Dim,T> &world)
{
    mConfig.setBoundingBox(world);
    mCurrentEvent.mBodyList = std::move(world.mBodyList);
}

} // namespace AstroGL
#endif
