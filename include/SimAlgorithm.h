#ifndef SIM_ALGORITHM_H__
#define SIM_ALGORITHM_H__

#include <vector>

namespace AstroCGL {

template<std::size_t d,typename t> class SimEngineConfig;
template<std::size_t d,typename t> class SimEvent;

template <std::size_t Dim,typename T>
class SimAlgorithm
{
public:
   virtual void beginSim(SimEngineConfig<Dim,T>* config)=0;
   virtual void processEvent(SimEvent<Dim,T>& event)=0; // do 1 step of simulation over this event
   virtual void endSim()=0;
};

template <std::size_t Dim,typename T>
using SimAlgorithmsList=std::vector< SimAlgorithm<Dim,T>* >;


} // namespace AstroCGL

#endif
