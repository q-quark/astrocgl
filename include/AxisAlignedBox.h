#ifndef _ASTROCGL_AXISALIGNEDBOX_H_
#define _ASTROCGL_AXISALIGNEDBOX_H_

#include <cmath>

#include "Point.h"

namespace AstroCGL
{

/*! \brief An Axis-Aligned Bounding Box for a \b N dimensional Box (a rectangle in 2D, a box in 3D, tesseract in 4D, etc..)
 *
 *  An \b N dimensional Box is also known as HyperCube.
 *
 * \tparam Dim number of dimensions (1,2,3,..)
 * \tparam Type data type of the coordinates (int,float,double,..)
 */
template <std::size_t Dim, typename Type>
class AxisAlignedBox
{
public:
    //! Default Constructor
    AxisAlignedBox();

    /*!
     * \brief Copy Constructor from otherBox
     * \param otherBox A different axis-aligned bounding box with the same dimensions and type
     */
    AxisAlignedBox(const AxisAlignedBox<Dim,Type>& otherBox);

    // explicit Constructors

    /*!
     * \brief construct a AxisAlignedBox from 2 Points diagonally oppossed
     * \param minPoint the lowest, minimum point of the Box
     * \param maxPoint the highest, maximum point of the Box
     *
     *  \sa class Point
     */
    explicit AxisAlignedBox(const Point<Dim,Type>& minPoint, const Point<Dim,Type>& maxPoint);

    /*!
     * \brief construct a AxisAlignedBox from values of the 2 Points diagonally oppossed
     *
     * The minPointValues & maxPointValues are used to setup the coordinates of the minimum and maximum points that
     * are diagonally oppossed.
     *
     * Such that, all coordinates of the minimum point are all set to minPointValues.
     * All coordinates of the maximum point are all set to maxPointValues.
     *
     * \param minPointValues the value used to setup the coordinates of the minimum point
     * \param maxPointValues the value used to setup the coordinates of the maximum point
     */
    explicit AxisAlignedBox(const Type& minPointValues, const Type& maxPointValues);


    static constexpr std::size_t dim() { return Dim; } //!< the number of dimensions (coordinates) this Box is defined in

    static constexpr std::size_t dimPoints() { return 1<<Dim; } //!< the number of Points required for this Box (4 in 2D, 8 in 3D, ..)

    static constexpr std::size_t dimArray() { return Dim*(1<<Dim); } //!< the required size of an array that can hold all coordinates of all the points of the box

    /*!
     * \brief get the minimum point of the box
     * \return the minimum point of the box
     *
     * \sa class Point
     */
    Point<Dim,Type> minPoint() const { return mMinPoint; } // the lowest corner

    /*!
     * \brief get the maximum point of the box
     * \return the maximum point of the box
     *
     * \sa class Point
     */
    Point<Dim,Type> maxPoint() const { return mMaxPoint; } // the highest corner

    /*!
     * \brief get the corner of the box with index \b i
     * \param i the index of a corner
     * \return a corner of the box
     *
     * \sa class Point
     */
    Point<Dim,Type> point(std::size_t i) const; // returns corner with index i of the box

    /*!
     * \brief get an array that holds all the corners of the box (hypercube)
     *
     * This method computes all corners of the box using the diagonally oppossed points minPoint() and maxPoint()
     *
     * The array contains all coordinates of all corners in an aligned way. Its size can be known with AxisAlignedBox::dimArray()
     *
     * \return an array that contains all coordinates of all corners of the cube
     *
     * \sa std::array, dimArray()
     */
    std::array<Type, AxisAlignedBox::dimArray()> array() const;

    /*!
     * \brief sets the minimum Point of the box
     * \param p A Point with the same number of coordinates and data types
     *
     * \sa class Point
     */
    void setMinPoint(const Point<Dim,Type>& p);

    /*!
     * \brief sets the maximum Point of the box
     * \param p A Point with the same number of coordinates and data types
     *
     * \sa class Point
     */
    void setMaxPoint(const Point<Dim,Type>& p);


    /*!
     * \brief sets the minimum & maximum Points of the box
     * \param minPoint the lowest, minimum Point of the box
     * \param maxPoint the highest, maximum Point of the box
     *
     *
     * \note The points must be diagonally oppossed and have the same dimensions (number of coordinates) and coordinates types as
     * the box.
     *
     * \sa class Point
     */
    void setPoints(const Point<Dim,Type>& minPoint, const Point<Dim,Type>& maxPoint);


    /*!
     * \brief check if the Point \b &p is contained inside this box
     * \param p A Point with the same number of coordinates and data types
     * \return true if the point is inside, false otherwise
     *
     * \sa class Point
     */
    bool contains(const Point<Dim,Type>& p) const;// returns true if point is inside this box

    /*!
     * \brief check if the \b bbox is contained inside this box
     * \param bbox Another box with the same number of coordinates and data types
     * \return true if the box is inside, false otherwise
     */
    bool contains(const AxisAlignedBox<Dim,Type>& bbox) const;// returns true if point is inside this box

    /*!
     * \brief check if point \b p is valid (number of dimensions, etc..)
     * \param p A Point with the same number of coordinates and data types
     * \return true if point passes the validity checks
     *
     * \sa class Point
     */
    bool isValidPoint(const Point<Dim,Type>& p) const;

    /*!
     * \brief  check if \b index is valid (index>=1 AND index<=# of points)
     * \param index a integer that counts a corner of the box
     * \return true if the index passes the validity checks
     */
    bool isValidIndex(std::size_t index) const;

private:
    Point<Dim,Type> mMinPoint; // the minimum point of the box
    Point<Dim,Type> mMaxPoint; // the maximum point of the box
};

template <std::size_t Dim, typename Type>
bool AxisAlignedBox<Dim,Type>::isValidPoint(const Point<Dim,Type>& p) const
{
    return true;
}

template <std::size_t Dim, typename Type>
bool AxisAlignedBox<Dim,Type>::isValidIndex(std::size_t index) const
{
    return index>1 && index<=dimPoints();
}

template <std::size_t Dim, typename Type>
void AxisAlignedBox<Dim,Type>::setMinPoint(const Point<Dim,Type>& p)
{
    mMinPoint = p;
}

template <std::size_t Dim, typename Type>
void AxisAlignedBox<Dim,Type>::setMaxPoint(const Point<Dim,Type>& p)
{
    mMaxPoint = p;
}

template<std::size_t Dim, typename Type>
void AxisAlignedBox<Dim,Type>::setPoints(const Point<Dim, Type> &minPoint, const Point<Dim, Type> &maxPoint)
{
    mMinPoint = minPoint;
    mMaxPoint = maxPoint;
}

template <std::size_t Dim, typename Type>
bool AxisAlignedBox<Dim,Type>::contains(const Point<Dim,Type>& p) const
{
    if(!isValidPoint(p)) return false;

    // Code to check if point is inside poly
    //
    // adapted from PNPOLY - by W.Randolph Franklin (https://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html)
    //    int i, j, c = 0;
    //    int nvert=8;
    //    for (i = 1, j = nvert-1; i < nvert; j=i, i+=2) {
    //        if ( ((mData[i]>p.array()[1])!=(mData[j]>p.array()[1])) &&
    //            (p.array()[0] < (mData[j-1]-mData[i-1]) * (p.array()[1]-mData[i]) / (mData[j]-mData[i]) + mData[i-1]) )
    //           c = !c;
    //    }

    return mMinPoint.array()<=p.array() && p.array()<=mMaxPoint.array(); // this works only for AxisAligned BoudingBox
}

template<std::size_t Dim, typename Type>
AxisAlignedBox<Dim,Type>::AxisAlignedBox()
    : mMinPoint(),
      mMaxPoint()
{

}

template<std::size_t Dim, typename Type>
AxisAlignedBox<Dim,Type>::AxisAlignedBox(const AxisAlignedBox<Dim,Type>& otherBox)
    : mMinPoint(otherBox.minPoint()),
      mMaxPoint(otherBox.maxPoint())
{

}

template<std::size_t Dim, typename Type>
AxisAlignedBox<Dim,Type>::AxisAlignedBox(const Point<Dim,Type>& minPoint, const Point<Dim,Type>& maxPoint)
    : mMinPoint(minPoint),
      mMaxPoint(maxPoint)
{

}

template<std::size_t Dim, typename Type>
AxisAlignedBox<Dim,Type>::AxisAlignedBox(const Type& minPointValues, const Type& maxPointValues)
    : mMinPoint(minPointValues),
      mMaxPoint(maxPointValues)
{

}


template <std::size_t Dim, typename Type>
std::array<Type, AxisAlignedBox<Dim,Type>::dimArray()> AxisAlignedBox<Dim,Type>::array() const
{
    std::array<Type, dimArray()> cornerPoints;

    Type cubeLength = mMaxPoint.array()[0] - mMinPoint.array()[0];

    cornerPoints.fill(0);

    for(std::size_t i=2*Dim-1, pointPos=2, coordPos=Dim-1; i<dimArray(); i+=Dim )
    {
        coordPos = i%Dim;
        pointPos = (i-coordPos)/Dim;

        for(int j=0, tempV=1; j<Dim; j++)
        {
            tempV = cornerPoints[i-j-Dim]+tempV;

            switch(tempV)
            {
            case 1:
            {
                cornerPoints[i-j] = 1;
                tempV = 0;
            }
                break;
            case 2:
            {
                cornerPoints[i-j] = 0;
                tempV = 1;
            }
                break;
            default:
            {
                cornerPoints[i-j] = 0;
                tempV = 0;
            }
            }
        }

    }

    return cornerPoints;
}


} // namespace AstroCGL
#endif
