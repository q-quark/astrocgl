#ifndef SIMPLEALGORITHM_H
#define SIMPLEALGORITHM_H

#include "InteractionField.h"
#include "SimAlgorithm.h"
#include "SimEvent.h"

namespace AstroCGL{

template <std::size_t Dim,typename T>
class SimpleAlgorithm : public SimAlgorithm<Dim,T>
{
public:
    SimpleAlgorithm()
        : mField(0),
    mConfigurator(0)
    {    }

    ~SimpleAlgorithm(){}

   void beginSim(SimEngineConfig<Dim,T>* config) override;
   void processEvent(SimEvent<Dim,T>& event) override;
   void endSim() override;

   void setInteractionField(const  InteractionField<Dim,T>* field) { mField = field; }

protected:
    const InteractionField<Dim,T>* mField;
    const SimEngineConfig<Dim,T>* mConfigurator;
};

template<std::size_t Dim, typename T>
void SimpleAlgorithm<Dim,T>::beginSim(SimEngineConfig<Dim, T>* config)
{
    mConfigurator = config;
}

template<std::size_t Dim, typename T>
void SimpleAlgorithm<Dim,T>::processEvent(SimEvent<Dim,T> &event)
{
    unsigned int numberOfBodies = event.bodiesListSize();

    for(int i=0; i<numberOfBodies; i++){
        SharedBody<Dim,T>& p1 = event.getBody(i);

        mConfigurator->odeSolver()->doStep(p1, mField);

    } // for bodies
}

template<std::size_t Dim, typename T>
void SimpleAlgorithm<Dim,T>::endSim()
{

}

}

#endif // SIMPLEALGORITHM_H
