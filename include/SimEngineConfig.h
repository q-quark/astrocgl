#ifndef SIMENGINECONFIG_H
#define SIMENGINECONFIG_H

#include "AxisAlignedBox.h"
#include "SimAlgorithm.h"

#include "QksODE.h"

namespace AstroCGL {

template <std::size_t Dim, typename T>
class SimEngineConfig
{
public:
    SimEngineConfig();
    virtual ~SimEngineConfig();

    double startTime() const;
    void setStartTime(double startTime);

    double endTime() const;
    void setEndTime(double endTime);

    int numberOfEvents() const;
    void setNumberOfEvents(int numberOfEvents);

    double timeAtStep(int stepIndex) const;

    AxisAlignedBox<Dim, T> boundingBox() const;
    void setBoundingBox(const AxisAlignedBox<Dim, T> &boundingBox);

    SimAlgorithmsList<Dim,T> algorithms() const;
    void addAlgorithm(SimAlgorithm<Dim,T>* alg);


    QksODESystem<Dim,T>& odeSolver() const
    {
        return mODESolver;
    }

protected:
    // Simulation details
    int mNumberOfEvents; // or Number of Steps
    double mStartTime;
    double mEndTime;

    SimAlgorithmsList<Dim,T> mAlgorithmsList; // algorithms used to do the simulation
    AxisAlignedBox<Dim,T> mWorldBox;
    QksODESystem<Dim,T> mODESolver;
};

template<std::size_t Dim, typename T>
SimEngineConfig<Dim,T>::SimEngineConfig()
    : mNumberOfEvents(100),
      mStartTime(0.0),
      mEndTime(0.0)
{

}

template<std::size_t Dim, typename T>
SimEngineConfig<Dim,T>::~SimEngineConfig()
{

}

template<std::size_t Dim, typename T>
double SimEngineConfig<Dim,T>::startTime() const
{
    return mStartTime;
}

template<std::size_t Dim, typename T>
void SimEngineConfig<Dim,T>::setStartTime(double startTime)
{
    mStartTime = startTime;
}

template<std::size_t Dim, typename T>
double SimEngineConfig<Dim,T>::endTime() const
{
    return mEndTime;
}

template<std::size_t Dim, typename T>
void SimEngineConfig<Dim,T>::setEndTime(double endTime)
{
    mEndTime = endTime;
}

template<std::size_t Dim, typename T>
AxisAlignedBox<Dim,T> SimEngineConfig<Dim,T>::boundingBox() const
{
    return mWorldBox;
}

template<std::size_t Dim, typename T>
void SimEngineConfig<Dim,T>::setBoundingBox(const AxisAlignedBox<Dim, T> &worldBox)
{
    mWorldBox = worldBox;
}

template<std::size_t Dim, typename T>
SimAlgorithmsList<Dim,T> SimEngineConfig<Dim,T>::algorithms() const
{
    return mAlgorithmsList;
}

template<std::size_t Dim, typename T>
void SimEngineConfig<Dim,T>::addAlgorithm(SimAlgorithm<Dim, T> *alg)
{
    mAlgorithmsList.push_back(alg);
}

template<std::size_t Dim, typename T>
int SimEngineConfig<Dim,T>::numberOfEvents() const
{
    return mNumberOfEvents;
}

template<std::size_t Dim, typename T>
void SimEngineConfig<Dim,T>::setNumberOfEvents(int numberOfEvents)
{
    if(numberOfEvents==0) numberOfEvents=1;

    mNumberOfEvents = numberOfEvents;
}

template<std::size_t Dim, typename T>
double SimEngineConfig<Dim,T>::timeAtStep(int stepIndex) const
{
    double timeN = mStartTime + stepIndex*(mEndTime-mStartTime)/(double)mNumberOfEvents;

    return timeN;
}

} // namespace AstroCGL



#endif // SIMENGINECONFIG_H
