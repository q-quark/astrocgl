/**
 ** This file is part of the AstroCGL project.
 ** Copyright 2017 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as
 ** published by the Free Software Foundation, either version 3 of the
 ** License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef _ASTROCGL_VECTOR_H_
#define _ASTROCGL_VECTOR_H_

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <cmath>
#include <numeric>

#include <initializer_list>

namespace AstroCGL
{

/*! \brief A \b N dimensional Physics Vector
 *
 *   This class implements an \b N dimensional Physics Vector.
 *
 * Explicit Constructors have been defined  for most common uses: 1D,2D,3D, such that
 *
 * ```
 * #include "Vector.h"
 *
 * using namespace AstroCGL;
 *
 * ...
 *
 * Vector<1,double> v1(7.32); // construct a vector in 1D
 *
 *
 *
 * Vector<2,int> v2(98, 104); // construct a vector in 2D
 *
 *
 *
 * Vector<3,float> v3(1.78, 0.0, -44.21); // construct a vector in 3D
 *
 * ```
 *
 * Also, common variations uses of Vector have already been defined, ready for you to use:
 *
 * ```
 * #include "Vector.h"
 *
 * // respectively for 2D vectors
 *
 * Vector2i v2i(1,-3); // 2D - integer coordinate types
 * Vector2f v2f(1.99,-3.56); // 2D - float types
 * Vector2d v2d(1.99e+17,-3.063e-6); // 2D - double coordinate types
 *
 * // respectively for 3D vectors
 *
 * Vector3i v3i(1,-3,92); // 3D - integer coordinate types
 * Vector3f v3f(1.99,-3.56,92.44); // 3D - float types
 * Vector3d v3d(1.99e+17,-3.063e-6, 8272.21); // 3D - double coordinate types
 *
 * ```
 *
 * \tparam N number of dimensions (coordinates)
 * \tparam T the data type of the coordiantes (float, int, double, etc..)
 */
template<std::size_t N, typename Type>
class Vector
{
public:
    //! Default constructor
    Vector();

    /*!
     * \brief Copy constructor
     * \param other another Vector to copy
     */
    Vector(const Vector<N,Type> &other);

    /*!
     * \brief Explicit Constructor for 1D
     * \param Vx value on coordinate x
     */
    explicit Vector(const Type &Vx);

    /*!
     * \brief Explicit Constructor for 2D
     *
     * \note Use this constructor only in 2D
     *
     * \param Vx value on coordinate x
     * \param Vy value on coordinate y
     */
    explicit Vector(const Type &Vx, const Type &Vy);

    /*!
     * \brief Explicit Constructor for 3D
     *
     *  \note Use this constructor only in 3D
     *
     * \param Vx value on coordinate x
     * \param Vy value on coordinate y
     * \param Vz value on coordinate z
     */
    explicit Vector(const Type &Vx, const Type &Vy, const Type &Vz);


    /*!
     * \brief constructor from initializer list
     * \param an initializer list
     */
    Vector(std::initializer_list<Type> list);

    /*!
     * \brief get the component on x axis
     * \return the component on x axis
     */
    Type x() const;

    /*!
     * \brief get the component on y axis
     * \return the component on y axis
     */
    Type y() const;

    /*!
     * \brief get the component on z axis
     * \return the component on z axis
     */
    Type z() const;

    /*!
     * \brief get the coordinates as a const c-style array.
     *
     *  the size of the array is N - number of dimensions the vector is constructed in.
     *
     * \return a const c-style array of the coordinates
     *
     * \sa dim()
     */
    const Type* data() const;

    /*!
     * \brief get the coordinates as a c-style array.
     *
     *  the size of the array is N - number of dimensions the vector is constructed in.
     *
     * \return a c-style array of the coordinates
     *
     * \sa dim()
     */
    Type* data();

    /*!
     * \brief get the coordinates as a const array.
     *
     *  the size of the array is N - number of dimensions the vector is constructed in.
     *
     * \return const array of the coordinates
     *
     * \sa dim()
     */
    const std::array<Type,N>* array() const;

    /*!
     * \brief get the coordinates as a array.
     *
     *  the size of the array is N - number of dimensions the vector is constructed in.
     *
     * \return array of the coordinates
     *
     * \sa dim()
     */
    std::array<Type,N>* array();

    /*!
     * \brief get the number of the coordinates this vector is defined in.
     *
     * This is the same as the template paramter N
     *
     * \return the number of the coordinates
     */
    std::size_t dim() const;

    /*!
     * \brief sets the component on x coordinate
     * \param X the new value of component on coordinate x
     */
    void setX(Type X);

    /*!
     * \brief sets the component on y coordinate
     * \param Y the new value of component on coordinate y
     */
    void setY(Type Y);

    /*!
     * \brief sets the component on z coordinate
     * \param Z the new value of component on coordinate z
     */
    void setZ(Type Z);

    /*!
     * \brief sets the components on X and Y coordinates
     * \param X the new value of component on coordinate x
     * \param Y the new value of component on coordinate y
     */
    void setXY(Type X, Type Y);

    /*!
     * \brief sets the components on X, Y and Z coordinates
     * \param X the new value of component on coordinate x
     * \param Y the new value of component on coordinate y
     * \param Z the new value of component on coordinate z
     */
    void setXYZ(Type X, Type Y, Type Z);


    /*!
     * \brief sets all the components from the initializer list
     *
     * ```
     * Vector<5,float> v;
     * v.set( {3.0, .0, -1.0, 8.7, .0} );
     * ```
     *
     * \param list an initializer list
     */
    void set(std::initializer_list<Type> list);

    // Vector's specific operations
    /*!
     * \brief check if the components of this vector have been set
     *
     * \return true if the components have different values then the default ones
     */
    bool isNull() const;

    /*!
     * \brief computes the dot product of \b this vector with the vector \b other
     * \param other A different Vector with the same size and type
     *
     * <b>The algebraic definition</b> of the dot product:
     *
     * \f$v \cdot u = \sum_{i=1}^{n} v_i u_i = v_1 u_1 + v_2 u_2 + \ldots v_n u_n\f$
     *
     * The result of the dot product of two Vectors is a scalar.
     *
     * \return the result of the dot product
     */
    Type dotProd(const Vector<N,Type>& other) const; // dot product

    /*!
     * \brief computes the cross product of \b this Vector with the Vector \b other
     * \param other A different Vector with the same size and type
     *
     *
     * \note Here, the cross product is implemented only in 3 dimensions. Its difficult to generalize it for higher dimensions.
     *
     * \return the result of the cross product
     */
    Vector<N,Type>& crossProd(const Vector<N,Type>& other) const; // cross product

    /*!
     * \brief computes the length (aka magnitude, norm or modulus) of this vector
     * \return the length of the vector
     */
    double length() const; // length, norm or magnitude of a vector

    /*!
     * \brief computes the squared length (aka squared magnitude, squared norm or squared modulus) of this vector
     * \return the squared length
     */
    Type lengthSqrd() const; // squared length, norm or magnitude of a vector

    /*!
     * \brief get the a normalized vector (aka unit vector) parallel with this vector
     *
     * The normalized vector of V is a vector in the same direction but with length (norm) 1.
     *
     * It is denoted \f$\hat{V}\f$ and given by \f$\hat{V}=\frac{V}{|V|}\f$,
     *
     * where \f$|V|\f$ is the norm of V. It is also called a <b>unit vector</b>.
     *
     * \return normalized vector
     */
    Vector<N,Type> normalized() const; // returns a unit vector along (parallel with) this vector

    /*!
     * \brief computes the cosine of the angle made with Vector other
     * \param other A different Vector with the same size and type
     * \return the result of the cosine of the angle made with Vector other
     */
    double cos(const Vector<N,Type>& other) const; // the cosine between these 2 vectors

    /*!
     * \brief computes the angle made with Vector other
     * \param other A different Vector with the same size and type
     * \return the angle made with Vector other
     */
    double angle(const Vector<N,Type>& other) const; // the angle between these 2 vectors

    /*!
     * \brief check if this vector is perpendicular with Vector other
     * \param other A different Vector with the same size and type
     * \return true if the vectors are perpendicular, false otherwise
     */
    bool isPerpendicular(const Vector<N,Type>& other) const; // true if perpendicular with other vector

    /*!
     * \brief computes the distance between this vector and other
     * \param other A different Vector with the same size and type
     * \return the distance between the vectors
     */
    double distance(const Vector<N,Type>& other) const;

    Vector<N,Type>& operator=(const Vector<N,Type> &other);

    const Type & operator[](std::size_t row) const;
    Type & operator[](std::size_t row);
protected:
    std::array<Type,N> mData; //!< stores the Vector's components
};

//////////////////////////////////////////////////////////////////////
//___________________________ Vector class implementation
//////////////////////////////////////////////////////////////////////
template <std::size_t N, typename Type>
Vector<N,Type>::Vector()
{
    std::fill_n(mData.begin(), N, Type());
}

template <std::size_t N, typename Type>
Vector<N,Type>::Vector(const Vector<N,Type> &other)
{
    mData = *(other.array());
}

template <std::size_t N, typename Type>
Vector<N,Type>::Vector(const Type &Vx)
{
    static_assert(N==1, "Constructor Vector(x) valid only in 1D");

    mData[0]=Vx;
}

template <std::size_t N, typename Type>
Vector<N,Type>::Vector(const Type &Vx, const Type &Vy)
{
    static_assert(N==2, "Constructor Vector(x,y) valid only in 2D");

    mData[0]=Vx;
    mData[1]=Vy;
}

template <std::size_t N, typename Type>
Vector<N,Type>::Vector(const Type &Vx, const Type &Vy, const Type &Vz)
{
    static_assert(N==3, "Constructor Vector(x,y,z) valid only in 3D");

    mData[0]=Vx;
    mData[1]=Vy;
    mData[2]=Vz;
}

template<std::size_t N, typename Type>
Vector<N,Type>::Vector(std::initializer_list<Type> list)
{
    std::copy_n(list.begin(), N, mData.begin());
}


// __________ Getters

template <std::size_t N, typename Type>
std::size_t Vector<N,Type>::dim() const
{ return N; }

template <std::size_t N, typename Type>
Type Vector<N,Type>::x() const
{
    static_assert(N>=1, "valid for at least in 1 Dimension");
    return mData[0];
}

template <std::size_t N, typename Type>
Type Vector<N,Type>::y() const
{
    static_assert(N>=2, "valid for at least in 2 Dimensions");

    return mData[1];
}

template <std::size_t N, typename Type>
Type Vector<N,Type>::z() const
{
    static_assert(N>=3, "valid for at least in 3 Dimensions");

    return mData[2];
}

template <std::size_t N, typename Type>
const Type* Vector<N,Type>::data() const
{ return mData.data(); }

template <std::size_t N, typename Type>
Type* Vector<N,Type>::data()
{ return mData.data(); }

template <std::size_t N, typename Type>
const std::array<Type,N>* Vector<N,Type>::array() const
{ return &mData; }

template <std::size_t N, typename Type>
std::array<Type,N>* Vector<N,Type>::array()
{ return &mData; }

// __________ Setters
template <std::size_t N, typename Type>
void Vector<N,Type>::setX(Type x)
{
    static_assert(N>=1, "valid for at least in 1 Dimensions");
    mData[0] = x;
}

template <std::size_t N, typename Type>
void Vector<N,Type>::setY(Type y)
{
    static_assert(N>=2, "valid for at least in 2 Dimensions");
    mData[1] = y;
}

template <std::size_t N, typename Type>
void Vector<N,Type>::setZ(Type z)
{
    static_assert(N>=3, "valid for at least in 3 Dimensions");
    mData[2] = z ;
}

template <std::size_t N, typename Type>
void Vector<N,Type>::setXY(Type x, Type y)
{
    static_assert(N>=2, "valid for at least in 2 Dimensions");
    mData[0] = x ; mData[1] = y;
}

template <std::size_t N, typename Type>
void Vector<N,Type>::setXYZ(Type x, Type y, Type z)
{
    static_assert(N>=3, "valid for at least in 3 Dimensions");
    mData[0] = x ; mData[1] = y; mData[2] = z;
}

template<std::size_t N, typename Type>
void Vector<N,Type>::set(std::initializer_list<Type> listCoords)
{
    std::copy_n(listCoords.begin(), N, mData.begin());
}

template <std::size_t N, typename Type>
bool Vector<N,Type>::isNull() const
{ return std::all_of(mData.begin(), mData.end(), [](Type i){ return i==Type(); } ); }

//_____________ Vector specific operations
template <std::size_t N, typename Type>
Type Vector<N,Type>::dotProd(const Vector<N,Type>& other) const
{
    return std::inner_product(mData.begin(), mData.end(), other.array().begin(), 0);
}

// cross product
template <std::size_t N, typename Type>
Vector<N,Type>& Vector<N,Type>::crossProd(const Vector<N,Type>& other) const
{
    static_assert(N==3, "valid only for 3 Dimensions");

    Vector<N,Type>* result = new Vector<N,Type>();

    result->setXYZ(
            mData[1]*other.data()[2]-mData[2]*other.data()[1],
            mData[2]*other.data()[0]-mData[0]*other.data()[2],
            mData[0]*other.data()[1]-mData[1]*other.data()[0]
            );

    return (*result);
}

// length, norm or magnitude of a vector
template <std::size_t N, typename Type>
double Vector<N,Type>::length() const
{
    return sqrt(std::inner_product(mData.begin(), mData.end(), mData.begin(), 0) );
}

template<std::size_t N, typename Type>
Type Vector<N,Type>::lengthSqrd() const
{
    return std::inner_product(mData.begin(), mData.end(), mData.begin(), 0);
}

// returns a uniT vector defined by this vector (vector of lenth 1 parallel with current vector)
template <std::size_t N, typename Type>
Vector<N, Type> Vector<N, Type>::normalized() const
{
    Type len = length();
    Type fLen = 1.0/len;

    Vector<N,Type> normVec;
    (normVec)*fLen;

    return (normVec);
} 

// the cosine between these 2 vectors
template <std::size_t N, typename Type>
double Vector<N,Type>::cos(const Vector<N,Type>& other) const
{
    return dotProd(other)/length()/other.length();
}

// the angle between these 2 vectors
template <std::size_t N, typename Type>
double Vector<N,Type>::angle(const Vector<N,Type>& other) const
{
    return acos( this->cos(other) );
}

// return true if perpendicular with 'other' vector
template <std::size_t N, typename Type>
bool Vector<N,Type>::isPerpendicular(const Vector<N,Type>& other) const
{
    return  this->cos(other)>=1e-6 ? false : true ;
}

template <std::size_t N, typename Type>
double Vector<N,Type>::distance(const Vector<N,Type>& other) const
{
    Vector<N,Type> diff = (*this) - other;

    return diff.length();
}


//////////////////////////////////////////////////////////////////////
//__________________________ Vector Operations
//////////////////////////////////////////////////////////////////////

//ASIGNMENT OPERATIONS
template <std::size_t N, typename Type>
Vector<N,Type>& Vector<N,Type>::operator=(const Vector<N,Type>& b)
{
    mData = *(b.array());

    return (*this);
}

template<std::size_t N, typename Type>
const Type& Vector<N,Type>::operator[](std::size_t row) const
{
    return mData[row];
}

template<std::size_t N, typename Type>
Type& Vector<N,Type>::operator[](std::size_t row)
{
    return mData[row];
}

// Vector TO Vector OPERATIONS
template <std::size_t N, typename Type>
Vector<N,Type>& operator+=(Vector<N,Type>& a, const Vector<N,Type>& b)
{  

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&b](Type &val){ val+=b.data()[i]; i++; } );

    return a;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator+(const Vector<N,Type>& a, const Vector<N,Type>& b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&z,&b](Type &val){ z.data()[i]=val+b.data()[i]; i++; } );

    return z;
}

template <std::size_t N, typename Type>
Vector<N,Type>& operator-=(Vector<N,Type>& a, const Vector<N,Type>& b)
{  
    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&b](Type &val){ val-=b.data()[i]; i++; } );

    return a;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator-(const Vector<N,Type>& a, const Vector<N,Type>& b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&z,&b](Type &val){ z.data()[i]=val-b.data()[i]; i++; } );

    return z;
}

// SCALAR TO VECTOR OPERATIONS
template <std::size_t N, typename Type>
Vector<N,Type> operator+(const Vector<N,Type>& a, Type b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&](const Type &val){ z.data()[i]=val+b; i++; } );

    return z;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator-(const Vector<N,Type>& a, Type b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&](const Type &val){ z.data()[i]=val-b; i++; } );

    return z;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator*(const Vector<N,Type>& a, Type b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&z,&b](const Type &val){ z.data()[i]=val*b; i++; } );

    return z;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator*(Type b, const Vector<N,Type>& a)
{
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&z,&b](const Type &val){ z.data()[i]=val*b; i++; } );

    return z;
}

template <std::size_t N, typename Type>
Vector<N,Type> operator/(const Vector<N,Type>& a, Type b)
{ 
    Vector<N,Type> z;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&z,&b](const Type &val){ z.data()[i]=val/b; i++; } );

    return z;
}

// EQUALITY OPERATIONS
template <std::size_t N, typename Type>
bool operator==(const Vector<N,Type>& a, const Vector<N,Type>& b)
{ 
    int i=0;

    return std::all_of(a.array()->begin(), a.array()->end(),
                       [&i,&b](const Type valA){
                            bool check = valA==b.data()[i];
                            i++;
                            return check;
                        } );
}

template <std::size_t N, typename Type>
bool operator!=(const Vector<N,Type>& a, const Vector<N,Type>& b)
{ 
    int i=0;

    return std::all_of(a.array()->begin(), a.array()->end(),
                       [&i,&b](const Type valA){
                            bool check = valA==b.data()[i];
                            i++;
                            return check;
                        } );
}

template <std::size_t N, typename Type>
double distance(const Vector<N,Type> &v, const Vector<N,Type> &u)
{
    Vector<N,Type> D = v-u;

    return D.length();
}

using Vector2i=Vector<2,int>; //!< 2D vector with element type integer
using Vector2f=Vector<2,float>; //!< 2D vector with element type float
using Vector2d=Vector<2,double>;//!< 2D vector with element type double

using Vector3i=Vector<3,int>; //!< 3D vector with element type integer
using Vector3f=Vector<3,float>; //!< 3D vector with element type float
using Vector3d=Vector<3,double>; //!< 3D vector with element type double

} // namespace AstroGL
#endif
