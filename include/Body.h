/**
 ** This file is part of the AstroCGL project.
 ** Copyright 2017 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as
 ** published by the Free Software Foundation, either version 3 of the
 ** License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef _ASTROCGL_BODY_H_
#define _ASTROCGL_BODY_H_

#include <memory>
#include <vector>

#include "Point.h"
#include "Vector.h"

namespace AstroCGL
{


//!  An interface for physical bodies (or particles).
/*!
*
* Classes must derive from this and implement the methods defined in this base class.
*
* \sa Some implementation examples: ClassicParticle, RelativisticParticle
*/
template < std::size_t Dim, typename T >
class Body
{
public:
// A derived class must implement these methods
// considering the impl methods to be protected and named with prefix "*"

    // Getters
    std::size_t dim() const { return Dim; }


    /*!
     * \brief the mass (rest mass) of this body
     *
     *
     * \return the mass of this body
     */
    virtual T mass() const =0;

    /*!
     * \brief the position of the body
     * \return position of the body as a AstroCGL::Point
     *
     * ```
     * ...
     *  Point<2,float> p = body.position();
     * ...
     * ```
     *
     * \sa Point
     */
    virtual Point<Dim,T> position() const =0;
    /*!
     * \brief the velocity of the body
     * \return  the velocity of the body as a AstroCGL::Vector
     */
    virtual Vector<Dim,T> velocity() const =0;

    /*!
     * \brief the momentum of the body
     * \return the momentum of the body as a AstroCGL::Vector
     */
    virtual Vector<Dim,T> momentum() const =0;

    /*!
     * \brief the energy of the body
     *
     *  NB: The returned energy is computed according to the implementation: classically or relativistic.
     *
     * \return the energy of the body
     *
     * \sa ClassicParticle, RelativisticParticle
     */
    virtual T energy() const =0;

    // Setters

    /*!
     * \brief sets the mass of the body
     * \param m the new mass of the body
     */
    virtual void setMass(T m) =0;


    virtual Point<Dim,T> position() =0;
    virtual Vector<Dim,T> velocity() =0;
    virtual Vector<Dim,T> momentum() =0;

    /*!
     * \brief sets the position of the body
     * \param new_pos the new position of the body
     */
    virtual void setPosition(const Point<Dim,T>& new_pos) =0;

    /*!
     * \brief sets the velocity of the body
     * \param new_vel the new velocity of the body
     */
    virtual void setVelocity(const Vector<Dim,T>& new_vel) =0;

    /*!
     * \brief sets the momentum of the body
     * \param new_momentum the new momentum of the body
     */
    virtual void setMomentum(const Vector<Dim,T>& new_momentum) =0;

    /*!
     * \brief sets the Kinetic Energy of the body
     * \param new_energy the new kinetic energy
     */
    virtual void setEnergy(T new_energy) =0;

//    virtual Body< Dim,T >& operator=(Body< Dim,T > const&) =0;
//    virtual Body< Dim,T >& operator=(Body< Dim,T >&&) =0;
};


/*! \brief implementation of Body<Dim,T> for a Classic Particle
 *
 * This class implements a classic - newtonian - nonrelativistic particle.
 *
 * \tparam Dim number of dimensions
 * \tparam T data type of the coordinates
 */
template <std::size_t Dim, typename T>
class ClassicParticle : public Body< Dim,T >
{
private:
    T mMass;
    Point<Dim,T> mPosition;
    Vector<Dim,T> mVelocity;

public:
    //! default constructor
    ClassicParticle() :
        mMass(),
        mPosition(),
        mVelocity()
    { }

    //! copy constructor
    ClassicParticle(ClassicParticle<Dim,T> const& other) :
        mMass(other.mass()),
        mPosition(other.position()),
        mVelocity(other.velocity())
    { }

    //! move constructor
    ClassicParticle(ClassicParticle<Dim,T>&& other) :
        mMass(std::move(other.mass())),
        mPosition(std::move(other.position())),
        mVelocity(std::move(other.velocity()))
    { }

    ~ClassicParticle()
    { }


    ClassicParticle<Dim,T>& operator=(ClassicParticle<Dim,T> const& other)
    {
        mMass = other.mass();
        mPosition = other.position();
        mVelocity = other.velocity();

        return *this;
    }

    ClassicParticle<Dim,T>& operator=(ClassicParticle<Dim,T>&& other)
    {
        mMass = std::move(other.mass());
        mPosition = std::move(other.position());
        mVelocity = std::move(other.velocity());

        return *this;
    }

    T mass() const { return mMass; }
    Point<Dim,T> position()  const { return mPosition; }
    Vector<Dim,T> velocity() const { return mVelocity; }
    Vector<Dim,T> momentum() const { return mMass*mVelocity; }


    /*!
     * \brief implements the Getter for the energy
     *
     * the Kinetic Energy is computed according to classic mechanics:
     * \f$E=\frac{m \cdot v^2}{2}\f$
     *
     * \return the kinetic energy
     */
    T energy() const { return mMass*mVelocity.lengthSqrd()/2.0;  }

    Point<Dim,T> position()  { return mPosition; }
    Vector<Dim,T> velocity() { return mVelocity; }
    Vector<Dim,T> momentum() { return mMass*mVelocity; }

    void setMass(T m) { mMass = m; }
    void setPosition(const Point<Dim,T>& new_pos) { mPosition = new_pos; }
    void setVelocity(const Vector<Dim,T>& new_vel){ mVelocity = new_vel; }

    /*!
     * \brief implements the setter for the momentum of a classic particle
     *
     * Setting the momentum will actually keep the mass constant (at the previous value) but change its the velocity
     *
     *  \f$\vec{v}=\frac{\vec{p}}{m}\f$
     *
     * \param new_momentum the new momentum vector of the particle
     */
    void setMomentum(const Vector<Dim,T>& new_momentum)
    {
        // mass is constant in Classic (Newtonian) Physics, thus we only set the velocity

        mVelocity = new_momentum/mMass;
    }

    /*!
     * \brief implements the setter for Kinetic Energy. This method modifies also the velocity (since the mass is a constant)
     *
     * the velocity is modified according to formula: \f$v = \sqrt{2 \cdot \frac{E}{m}}\f$,  but the direction is kept along the previous direction
     *
     * \param new_energy - the new value of the Kinetic Energy
     */
    void setEnergy(T new_energy)
    {
        // mass is constant in Classic (Newtonian) Physics
        // but we have different Kinetic energy, so only v has changed..

        T new_v = (T)sqrt(2.0*new_energy/mMass); // but v is scalar

        // so, we keep the same direction
        Vector<Dim,T> normV = mVelocity.normalized();

        // set new velocity in the same direction
        mVelocity =  normV*new_v;
    }


};

/*! \brief For the moment this class is not implemented. DO NOT USE!
 *
 *  Its here for completion purposes. It may be implemented in the near future
 *
 *  \note For the moment this class is not implemented. DO NOT USE!
 */
template <std::size_t Dim, typename T>
class RelativisticParticle : public Body< Dim,T >
{
private:
    T mMass;

protected:
    T mass() const { return mMass; }
    void setMass(T m) { mMass = sqrt(m); }

};

template <std::size_t Dim, typename T>
using BodyList=std::vector< Body<Dim,T> >; //!< A List containing Bodies

// Typedefs & Aliases
template <std::size_t Dim, typename T>
using SharedBody=std::shared_ptr< Body<Dim,T> >; //!< shared pointer to a Body

template <std::size_t Dim, typename T>
using SharedBodyList=std::vector< SharedBody<Dim,T> >; //!< A List containing Shared Bodies

}// namespace AstroGL

#endif
