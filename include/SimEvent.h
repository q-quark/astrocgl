#ifndef SIMEVENT_H__
#define SIMEVENT_H__

#include "Body.h"
#include "SimEngine.h"

namespace AstroCGL {

template <std::size_t Dim, typename Type>
class SimEvent
{
    // friends that can modify members
    template<std::size_t D, typename T> friend class SimEngine;

public:
    SimEvent();
    virtual ~SimEvent();

    int stepIndex() const;
    size_t bodiesListSize() const;
    SharedBody<Dim,Type>& getBody(int i);

protected:
    int mStepIndex;
    SharedBodyList<Dim,Type> mBodyList;
};

template<std::size_t Dim, typename Type>
SimEvent<Dim,Type>::SimEvent()
    : mStepIndex(-1)
{

}

template<std::size_t Dim, typename Type>
SimEvent<Dim,Type>::~SimEvent()
{

}

template<std::size_t Dim, typename Type>
int SimEvent<Dim,Type>::stepIndex() const
{
    return mStepIndex;
}

template<std::size_t Dim, typename Type>
size_t SimEvent<Dim, Type>::bodiesListSize() const
{
    return mBodyList.size();
}

template<std::size_t Dim, typename Type>
SharedBody<Dim,Type>& SimEvent<Dim, Type>::getBody(int i)
{
    return mBodyList.at(i);
}



} // namespace AstroCGL


#endif // SIMEVENT_H
