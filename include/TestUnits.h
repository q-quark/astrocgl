#ifndef TEST_UNITS_H_
#define TEST_UNITS_H_ 

#include <iostream>

#include "Point.h"
#include "Vector.h"

namespace AstroCGL
{

template <typename T>  
std::ostream& operator<<(std::ostream& stream, const Point<2,T> &point)
{
	stream << "Point2D(" << point.x() << " , " << point.y() << ")" << std::endl;

	return stream;
}

// STREAMERS
template<class T>
std::ostream& operator<<(std::ostream &os, const Vector<3,T>&v)
{
    os << v.x() << " " << v.y() << " " << v.z();

    return os;
}

template<class T>
std::istream& operator>>(std::istream& is, Vector<3,T>& v)
{
    T value;
    is >> value;
    v = v+value;
    return is;
}

} // namespace AstroGL

#endif
