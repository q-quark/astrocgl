/**
 ** This file is part of the AstroCGL project.
 ** Copyright 2017 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as
 ** published by the Free Software Foundation, either version 3 of the
 ** License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef _ASTROCGL_POINT_H_
#define _ASTROCGL_POINT_H_

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <cmath>
#include <numeric>
#include <initializer_list>

namespace AstroCGL {

template <std::size_t N, typename T> class Vector;


/*! \brief A "N" dimensional point
 *
 *  This class implements an "N" dimensional point.
 *
 * Explicit Constructors have been defined  for most common uses: 1D,2D,3D, such that
 *
 * ```
 * #include "Point.h"
 *
 * using namespace AstroCGL;
 *
 * ...
 *
 * Point<1,double> p1(7.32); // construct a point in 1D
 *
 *
 *
 * Point<2,int> p2(98, 104); // construct a point in 2D
 *
 *
 *
 * Point<3,float> p3(1.78, 0.0, -44.21); // construct a point in 3D
 *
 * ```
 *
 * Also, common variations uses of Point have already been defined, ready for you to use:
 *
 * ```
 * #include "Point.h"
 *
 * // respectively for 2D points
 *
 * Point2i p2i(1,-3); // 2D - integer coordinate types
 * Point2f p2f(1.99,-3.56); // 2D - float types
 * Point2d p2d(1.99e+17,-3.063e-6); // 2D - double coordinate types
 *
 * // respectively for 3D points
 *
 * Point3i p3i(1,-3,92); // 3D - integer coordinate types
 * Point3f p3f(1.99,-3.56,92.44); // 3D - float types
 * Point3d p3d(1.99e+17,-3.063e-6, 8272.21); // 3D - double coordinate types
 *
 * ```
 *
 * \tparam N number of dimensions (coordinates)
 * \tparam T the data type of the coordiantes (float, int, double, etc..)
 */
template <std::size_t N, typename T>
class Point
{
public:
    /*!
     * \brief Default constructor
     */
    Point()
    {
        std::fill_n(mData.begin(), N, T());
    }

    /*!
     * \brief Default constructor
     */
    explicit Point(const T& val)
    {
        std::fill_n(mData.begin(), N, val);
    }

    /*!
     * \brief Copy Constructor
     * \param Construct a point from the value of the point other
     */
    Point(const Point<N,T>& other)
    {
        mData = *(other.array());
    }


    /*!
     * \brief constructor from initializer list
     *
     * ```
     * Point<3,float> p = {2.0, .0, -15.0};
     * ```
     *
     * \param an initializer list
     */
    Point(std::initializer_list<T> list);

    /*!
     * \brief explicit constructor for a 2-dimensional Point
     *
     * Use this constructor when template parameter N=2
     *
     * \param pX - the value of the coordinate on "x" coordinate
     * \param pY - the value of the coordinate on "y" coordinate
     */
    explicit Point(const T& pX, const T& pY) :
        mData{pX,pY}
    {
        static_assert(N == 2, "(X,Y) constructor only usable in 2D");
    }

    /*!
     * \brief explicit constructor for a 3-dimensional Point
     *
     * Use this constructor when template parameter N=3
     *
     * \param pX - the value of the coordinate on "x" coordinate
     * \param pY - the value of the coordinate on "y" coordinate
     * \param pZ - the value of the coordinate on "z" coordinate
     */
    explicit Point(const T& pX, const T& pY, const T& pZ) :
        mData{pX, pY, pZ}
    {
        static_assert(N == 3, "(X,Y,Z) constructor only usable in 3D");
    }

    /*!
     * \brief get the coordinates as a c-style array.
     *
     *  the size of the array is N - number of dimensions the point is constructed in.
     *
     * \return a const c-style array of the coordinates
     *
     * \sa dim()
     */
    const T* data() const;

    /*!
     * \brief get the coordinates as a c-style array.
     *
     *  the size of the array is N - number of dimensions the point is constructed in.
     *
     * \return a c-style array of the coordinates
     *
     * \sa dim(), array()
     */
    T* data();

    /*!
     * \brief get the coordinates as a const array
     *
     * the size of the array is N - number of dimensions the point is constructed in,
     * and can be returned bu dim() or by std::array::size()
     *
     * \return a const array containing the value of the coordinates of this point
     * \sa dim(), data()
     */
    const std::array<T,N>* array() const;

    /*!
     * \brief get the coordinates as an array
     *
     * the size of the array is N - number of dimensions the point is constructed in,
     * and can be returned bu dim() or by std::array::size()
     *
     * \return an array containing the value of the coordinates of this point
     * \sa dim(), data()
     */
    std::array<T,N>* array();

    /*!
     * \brief get the number of the coordinates this point.
     *
     * This is the same as the template paramter N
     *
     * \return the number of the coordinates
     */
    std::size_t dim() const;

    /*!
     * \brief check if the coordinates have been set
     *
     * \return true if the coordinates are different then the default values
     */
    bool isNull() const;

    /*!
     * \brief get the manhattan distance from origin to this point
     *
     *  The Manhattan distance is the simple sum of all coordinates.
     *
     * ie:
     * In 2D: d = x + y
     *
     * In 3D: d = x + y + z
     *
     * \return manhattan distance
     *
     * \sa length(), distance()
     */
    T manhattanLength() const;

    /*!
     * \brief get the distance (Pythagorean) from origin to this point
     *
     * The Pythagorean distance is defined:
     *
     * In 2D: \f$d^2 = x^2 + y^2\f$
     *
     * In 3D: \f$d^2 = x^2 + y^2 + z^2\f$
     *
     * \return the distance from origin to this point
     *
     * \sa manhattanLength(), distance()
     */
    T length() const;

    /*!
     * \brief get the distance from \b this point to point \b other
     * \param other the end Point to compute the distance to
     * \return the distance from current point to other
     *
     * \sa length(), manhattanLength()
     */
    double distance(const Point<N,T>& other) const;

    /*!
     * \brief move this point by this vector
     *
     * The Vector dv must have the same dimensions (N) and type (T) as this point
     *
     * \param dv Vector used for moving the point
     *
     * \sa Vector
     */
    void moveBy(const Vector<N,T>& dv);

    /*!
     * \brief get the coordinate x
     *
     * This method is only available only when N>=1
     *
     * \return the value of the coordinate x
     */
    T x() const; // returns the x coordinate

    /*!
     * \brief get the coordinate y
     *
     * This method is only available only when N>=2
     *
     * \return the value of the coordinate y
     */
    T y() const; // returns the y coordinate

    /*!
     * \brief get the coordinate z
     *
     * This method is only available only when N>=3
     *
     * \return the value of the coordinate z
     */
    T z() const; // returns the z coordinate

    /*!
     * \brief sets the x coordinate
     * \param x the new value of coordinate x
     */
    void setX(T x);

    /*!
     * \brief sets the y coordinate
     * \param y the new value of coordinate y
     */
    void setY(T y);

    /*!
     * \brief sets the z coordinate
     * \param z the new value of coordinate z
     */
    void setZ(T z);

    /*!
     * \brief sets the coordinates x and y
     *
     * Use this method only when in 2D
     *
     * \param x the new value of coordinate x
     * \param y the new value of coordinate y
     */
    void setXY(T x, T y);

    /*!
     * \brief sets the coordinates x, y and z
     *
     * Use this method only when in 3D
     *
     * \param x the new value of coordinate x
     * \param y the new value of coordinate y
     * \param z the new value of coordinate z
     */
    void setXYZ(T x, T y, T z);

    /*!
     * \brief sets all the coordinates from the initializer list
     *
     * ```
     * Point<5,float> p;
     * p.set( {3.0, .0, -1.0, 8.7, .0} );
     * ```
     *
     * \param listCoords an initializer list
     */
    void set(std::initializer_list<T> listCoords);

    const T & operator[](std::size_t row) const;
    T & operator[](std::size_t row);

private:
    std::array<T,N> mData;
};

template <std::size_t N, typename T>
std::size_t Point<N,T>::dim() const
{ return N; }

template <std::size_t N, typename T>
T Point<N,T>::x() const
{
    static_assert(N >= 1, "x() method only usable at least in 1D");

    return mData[0];
}

template <std::size_t N, typename T>
T Point<N,T>::y() const
{
    static_assert(N >= 2, "y() method only usable at least in 2D");

    return mData[1];
}

template <std::size_t N, typename T>
T Point<N,T>::z() const
{
    static_assert(N >= 3, "z() method only usable at least in 3D");

    return mData[2];
}

template <std::size_t N, typename T>
Point<N,T>::Point(std::initializer_list<T> list)
{
    std::copy_n(list.begin(), N, mData.begin());
}

template <std::size_t N, typename T>
const T* Point<N,T>::data() const
{ return mData.data(); }

template <std::size_t N, typename T>
T* Point<N,T>::data()
{ return mData.data(); }

template<std::size_t N, typename T>
const std::array<T,N>* Point<N, T>::array() const
{
    return &mData;
}

template<std::size_t N, typename T>
std::array<T, N>* Point<N,T>::array()
{
    return &mData;
}

template <std::size_t N, typename T>
void Point<N,T>::setX(T x)
{
    static_assert(N >= 1, "setX() method only usable at least in 1D");
    mData[0] = x;
}

template <std::size_t N, typename T>
void Point<N,T>::setY(T y)
{
    static_assert(N >= 2, "setY() method only usable at least in 2D");
    mData[1] = y ;
}

template <std::size_t N, typename T>
void Point<N,T>::setZ(T z)
{
    static_assert(N >= 3, "setZ() method only usable at least in 3D");
    mData[2] = z ;
}

template <std::size_t N, typename T>
void Point<N,T>::setXYZ(T x, T y, T z)
{
    static_assert(N < 3, "setXYZ() method only usable at least in 3D");

    mData[0] = x ; mData[1] = y; mData[2] = z;
}

template<std::size_t N, typename T>
void Point<N,T>::set(std::initializer_list<T> listCoords)
{
    std::copy_n(listCoords.begin(), N, mData.begin());
}

template<std::size_t N, typename T>
const T &Point<N,T>::operator[](std::size_t row) const
{
    return mData[row];
}

template<std::size_t N, typename T>
T &Point<N,T>::operator[](std::size_t row)
{
    return mData[row];
}

template <std::size_t N, typename T>
bool Point<N,T>::isNull() const
{
    return std::all_of(mData.begin(), mData.end(), [](T i){ return i==T(); } );
}

template <std::size_t N, typename T>
T Point<N,T>::manhattanLength() const
{ return std::accumulate(mData.begin(), mData.end(), 0); }

template <std::size_t N, typename T>
T Point<N,T>::length() const
{ return sqrt( std::inner_product(mData.begin(), mData.end(), mData.begin(), 0) ); }

template <std::size_t N, typename T>
double Point<N,T>::distance(const Point<N,T>& other) const
{
    return sqrt( std::inner_product(mData.begin(), mData.end(), other.begin(), 0) );
}

template <std::size_t N, typename T>
void Point<N,T>::moveBy(const Vector<N,T>& dv)
{
    int i=0;
    std::for_each(mData.begin(), mData.end(), [&i,&dv](T &val){ val+=dv.data()[i]; i++; } );
}

template <std::size_t N, typename Type>
Vector<N,Type> operator-(const Point<N,Type>& a, const Point<N,Type>& b)
{
    Vector<N,Type> vec;

    int i=0;
    std::for_each(a.array()->begin(), a.array()->end(), [&i,&vec,&b](const Type &val){ vec.data()[i]=val-b.data()[i]; i++; } );

    return vec;
}

// EQUALITY OPERATIONS
template <std::size_t N, typename Type>
bool operator==(const Point<N,Type>& a, const Point<N,Type>& b)
{
    int i=0;

    return std::all_of(a.array()->begin(), a.array()->end(),
                       [&i,&b](const Type valA){
                            bool check = valA==b.data()[i];
                            i++;
                            return check;
                        } );
}

template <std::size_t N, typename Type>
bool operator!=(const Point<N,Type>& a, const Point<N,Type>& b)
{
    int i=0;

    return std::all_of(a.array()->begin(), a.array()->end(),
                       [&i,&b](const Type valA){
                            bool check = valA==b.data()[i];
                            i++;
                            return check;
                        } );
}

using Point2i=Point<2,int>; //!< 2D point with element type integer
using Point2f=Point<2,float>; //!< 2D point with element type float
using Point2d=Point<2,double>; //!< 2D point with element type double

using Point3i=Point<3,int>; //!< 3D point with element type integer
using Point3f=Point<3,float>; //!< 3D point with element type float
using Point3d=Point<3,double>; //!< 3D point with element type double

} // namespace AstroCGL
#endif
