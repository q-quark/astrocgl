#ifndef NODE_BH_H__
#define NODE_BH_H__ 

// # of bodies that can be stored in this node
#define NODE_CAPACITY 1 

#include <memory>

#include "AxisAlignedBox.h"
#include "Point.h"
#include "Body.h"

namespace AstroCGL
{

template <typename Type>
class NodeBH
{
public:
	NodeBH(const NodeBH<Type>* parent, const BaseBox<Type>& box);
	~NodeBH();

	BaseBox<Type>* boundingbox() const;
	void subdivide();

protected:
	NodeBH();
	NodeBH<Type>& operator=(const NodeBH<Type>& node);

	unsigned int mDim; // # of dimensions this node is defined in (2D,3D, ...)
	bool mIsActive;
	Type mMass; // total mass in this node
	BasePoint<Type>* mCenterOfMass; // the position of the CM
	BaseBox<Type>* mBox; // region that this node represents
	std::shared_ptr<BaseBody<Type>> mParticle; // the particle(s) contained in the box

	NodeBH<Type>* mParent;
	NodeBH<Type>* mChildren;

	friend class TreeBH<Type>;
};

} // namespace AstroGL
#endif
