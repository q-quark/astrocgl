/**
 ** This file is part of the AstroCGL project.
 ** Copyright 2017 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as
 ** published by the Free Software Foundation, either version 3 of the
 ** License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef _ASTROCGL_WORLD_H_
#define _ASTROCGL_WORLD_H_

#include <vector>
#include <memory>
#include <algorithm>

#include "Body.h"
#include "AxisAlignedBox.h"

namespace AstroCGL {


/*! \brief implements the Universe to do the simulation
 *
 * \tparam Dim number of spatial dimension to use (2, 3, ..)
 * \tparam T data type of the coordinates (Plain Old Data: float, int, double, etc..)
 * \tparam ParticlePhysicsType an implementation of Body (ClassicParticle, RelativisticParticle,..)
 */

template <std::size_t Dim, typename T>
class World : public AxisAlignedBox<Dim,T>
{
    template<std::size_t D, typename Tp> friend class SimEngine;
public:
    /*!
     * \brief default constructor
     */
    World();

    /*!
     * \brief construct a World having its bounding box defined by 2 diagonally opposed Points
     * \param minPoint the minimum Point of the world's bounding-box
     * \param maxPoint the maximum Point of the world's bounding-box
     *
     * \sa AxisAlignedBox, Point
     */
    World(const Point<Dim,T>& minPoint, const Point<Dim,T>& maxPoint);

    /*!
     * \brief construct a World having its bounding-box defined by 2 diagonally opposed Points
     * \param minPointValues a value for all coordinates of the minimum Point of the world's bounding-box
     * \param maxPointValues a value for all coordinates of the maximum Point of the world's bounding-box
     *
     * \sa AxisAlignedBox, Point
     */
    World(const T& minPointValues, const T& maxPointValues);

    /*!
     * \brief adds a  uniqueBody to the World.
     *
     *  The added body must have the same dimensions and type as the World it is added. The World will take ownership of the added body
     *
     * \param uniqueBody a std::shared_ptr to a pointer of type (or derived from) Body
     *
     * \sa class Body, class ClassicParticle, class RelativisticParticle
     */
    void addBody( const SharedBody<Dim,T> body );

    /*!
     * \brief fetch a registered body from this World
     *
     * The body is still owned by the World
     *
     * \note You can modify the body's properties by you may not delete it
     *
     * \param i the body with index "i"
     * \return a reference to a body from this World
     *
     * \sa Body
     */
    SharedBody<Dim,T> body(std::size_t i);

    /*!
     * \brief removes and deletes all bodies managed by this World
     */
    void removeAllBodies();

    /*!
     * \brief get the total number of bodies managed by this World
     * \return the number of bodies managed by this World
     */
    std::size_t numberOfBodies() const;

    /*!
     * \brief check if \b body is managed by this World
     * \param body A Body or derived Body defined in the same dimensions, dimensions type and Physics Type
     * \return true if the body is managed, false otherwise
     *
     * \sa Body, ClassicParticle, RelativisticParticle
     */
    bool contains(const SharedBody<Dim,T> &uniqueBody) const;

    /*!
     * \brief appends the contents of bodyList to the current list
     *
     * \param bodyList A List that contains shared pointers to Body or its derivatives (ClassicParticle or RelativisticParticle)
     */
    void addBodyList(SharedBodyList<Dim,T>& bodyList);

    /*!
     * \brief sets the current bodyList to this new list
     *
     * \note This performs a std::move() operation on the list. The current list is cleared first.
     * After this, the World will have the list's content and bodyList will be empty
     *
     * \note The current list is emptied first, and the current bodies will be freed
     *
     * \param newlist A List that contains shared pointers to Body or its derivatives (ClassicParticle or RelativisticParticle)
     */
    void setBodyList(SharedBodyList<Dim,T>& newlist);

protected:
    SharedBodyList<Dim,T> mBodyList; //!< a list with all the Bodies managed by this world

};

//_____________________________ IMPLEMENTATION ________________________________________________________________________________
template <std::size_t Dim, typename T>
World<Dim,T>::World()
    : AxisAlignedBox<Dim,T>::AxisAlignedBox(),
      mBodyList()
{

}

template <std::size_t Dim, typename T>
World<Dim,T>::World(const Point<Dim,T>& minPoint, const Point<Dim,T>& maxPoint)
    : mBodyList(),
      AxisAlignedBox<Dim,T>::AxisAlignedBox(minPoint,maxPoint)
{

}

template <std::size_t Dim, typename T>
World<Dim,T>::World(const T& minPointValues, const T& maxPointValues)
    : mBodyList(),
      AxisAlignedBox<Dim,T>::AxisAlignedBox(minPointValues,maxPointValues)
{

}

template <std::size_t Dim, typename T>
void World<Dim,T>::addBody(const SharedBody<Dim, T> body)
{
   mBodyList.push_back( body );
//     mBodyList.push_back( body );

}

template <std::size_t Dim, typename T>
SharedBody<Dim, T> World<Dim, T>::body(std::size_t i)
{
    return mBodyList[i];
}

template<std::size_t Dim, typename T>
void World<Dim,T>::removeAllBodies()
{
    mBodyList.clear(); // this should also delete the pointers (and if references reached 0, then it deallocates memory)
}

template <std::size_t Dim, typename T>
std::size_t World<Dim,T>::numberOfBodies() const
{
    return mBodyList.size();
}

template<std::size_t Dim, typename T>
bool World<Dim,T>::contains(const SharedBody<Dim, T> &uniqueBody) const
{
    return std::any_of(mBodyList.begin(), mBodyList.end(), [&uniqueBody](const auto &b)
    {
        return b==uniqueBody;
    });
}

template<std::size_t Dim, typename T>
void World<Dim,T>::addBodyList(SharedBodyList<Dim, T> &bodyList)
{
    std::copy(bodyList.begin(),bodyList.end(), mBodyList.end());
}

template<std::size_t Dim, typename T>
void World<Dim,T>::setBodyList(SharedBodyList<Dim, T> &newlist)
{
    mBodyList.clear();
    mBodyList = std::move(newlist);
}



} // namespace AstroCGL
#endif // WORLD_H
