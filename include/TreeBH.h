#ifndef TREE_BH_H__
#define TREE_BH_H__ 

#include <memory>

#include "NodeBH.h"

namespace AstroCGL
{

//_____________ A Barnes-Hut Tree in 'ndim' dimensions
template <typename Type>
class TreeBH
{
public:
	TreeBH(unsigned int ndim); // create a tree for this number of dimensions

	unsigned int dim() const;

	BaseBox<Type>& boundingBox() const; // box of the root node

	void setBoundingBox(const BaseBox<Type>* box);


	void insertBody( const std::shared_ptr<BaseBody<Type>>& bodyPtr); // recursively insert this body to its specific node

protected:
	TreeBH(); // not implemented
	TreeBH(const TreeBH<Type>& other); // not implemented
	TreeBH<Type>& operator=(const TreeBH<Type>& other); // not implemented

	NodeBH<Type>* mRoot; // the root node
};




} //namespace AstroGL
#endif
