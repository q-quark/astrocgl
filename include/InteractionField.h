#ifndef INTERACTIONFIELD_H
#define INTERACTIONFIELD_H

#include "Body.h"
#include "Vector.h"

namespace AstroCGL{

template <std::size_t Dim,typename T>
class InteractionField
{
public:
//   virtual void operator()( SharedBodyList<Dim,T>& bodies )=0;
   virtual void operator()( SharedBody<Dim,T>& body, Vector<Dim,T>& acceleration ) const=0;

};

template <std::size_t Dim,typename T>
class FieldNewton : public InteractionField<Dim,T>
{

    const SharedBodyList<Dim,T>& mInteractionBodies;

public:
    FieldNewton(SharedBodyList<Dim,T>& bodies) : mInteractionBodies(bodies) { }
    virtual ~FieldNewton() { }

    static T GravConst; // the gravitational constant

    // outputs acceleration exerted on body (from all bodies in the SharedBodyList)
    virtual void operator()( SharedBody<Dim,T>& body, Vector<Dim,T>& acceleration ) const
    {
        Vector<Dim,T> distanceBetweenBodies;
        T distanceSqrd;
        T potential;

        for(int j=0; j<mInteractionBodies.size(); j++)
        {
            SharedBody<Dim,T>& bodyJ = mInteractionBodies.at(j);
            distanceBetweenBodies = bodyJ->position() - body->position();
            distanceSqrd = distanceBetweenBodies.lengthSqrd();

            potential = GravConst*bodyJ->mass()/distanceSqrd;

            acceleration += potential*distanceBetweenBodies.normalized();
        }

        // acceleration is the output
    }
};

// initialize the gravitational constant
template  <std::size_t Dim,typename T>
T FieldNewton<Dim,T>::GravConst = 1.0;

}




#endif // INTERACTIONFIELD_H
